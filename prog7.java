package prog7;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class prog7 {
	public static void main(String[] args) {				
	myFrame okno = new myFrame(); 		
	}
}

class myFrame extends JFrame
{
	public myFrame()
	{
		myPanel panel = new myPanel(); 
		Container cont = getContentPane();
		cont.add(panel); 
		setBounds(0, 0, 1000, 700); 
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
		setVisible(true);
	}	
}

class myPanel extends JPanel
{
	private int k=0, f=0, t=1;
	
	public myPanel()
	{
		Timer tm = new Timer(10, new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent e) {
				if (k==255)
				{
					t++;
					if (t==7) t=1;
					f=1;
				}
				if (k==0) f=0;
				if (f==0) k++;
				if (f==1) k--;
				repaint();
			}
		});
		tm.start();
	}
	
	public void paintComponent(Graphics gr) {
		super.paintComponent(gr);

        for (int i=0; i<256; i++)
        {
        	gr.setColor(new Color(0,i,0));
        	if (t==1) gr.fillRect(i+k, i+k, 256-i, 256-i);
        	else gr.fillRect(i, i, 256-i, 256-i);
        	
        	gr.setColor(new Color(i,0,0));
        	if (t==2) gr.fillRect(i+256, i+k, 256-i, 256-i);
        	else gr.fillRect(i+256, i, 256-i, 256-i);
        	
        	gr.setColor(new Color(0,0,i));
        	if (t==3) gr.fillRect(i+512-k, i+k, 256-i, 256-i);
        	else gr.fillRect(i+512, i, 256-i, 256-i);
        	
        	gr.setColor(new Color(i,i,0));
        	if (t==4) gr.fillRect(i+k, i+256-k, 256-i, 256-i);
        	else gr.fillRect(i, i+256, 256-i, 256-i);
        	
        	gr.setColor(new Color(0,i,i));
        	if (t==5) gr.fillRect(i+256, i+256-k, 256-i, 256-i);
        	else gr.fillRect(i+256, i+256, 256-i, 256-i);
        	
        	gr.setColor(new Color(i,0,i));
        	if (t==6) gr.fillRect(i+512-k, i+256-k, 256-i, 256-i);
        	else gr.fillRect(i+512, i+256, 256-i, 256-i);
        }
		
	}
}


