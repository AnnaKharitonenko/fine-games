package prog8;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class prog8 {
	public static void main(String[] args) {				
	      new myFrame(); 		
	}
}

class myFrame extends JFrame
{
	public myFrame()
	{
		myPanel panel = new myPanel(); 
		Container cont = getContentPane();
		cont.add(panel); 
		
		Dimension dm = Toolkit.getDefaultToolkit().getScreenSize();
		int w = dm.width;
		int h = dm.height;
		
		setUndecorated(true);
		setBounds(0, 0, w, h);
		setAlwaysOnTop(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
		setVisible(true);
	}	
}

class myPanel extends JPanel
{
	private int w,h;
	public myPanel()
	{
		Dimension dm = Toolkit.getDefaultToolkit().getScreenSize();
		w = dm.width;
		h = dm.height;

		Timer tm = new Timer(50, new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent e) {
				repaint();
			}
		});
		tm.start();
	}
	
	public void paintComponent(Graphics gr) {
		super.paintComponent(gr);

		for (int i=0; i<w; i++)
		{
			for (int j=0; j<h; j++)
			{
		        gr.setColor(new Color((int)(Math.random()*255),(int)(Math.random()*255),(int)(Math.random()*255)));
		        gr.fillRect(i, j, 1, 1);
			}			
		}
		
	}
}


