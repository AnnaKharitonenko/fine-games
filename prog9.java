package prog9;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;

import javax.swing.*;

public class prog9 {
	public static void main(String[] args) {				
	      new myFrame(); 		
	}
}

class myFrame extends JFrame
{
	public myFrame()
	{
		myPanel panel = new myPanel(); 
		Container cont = getContentPane();
		cont.add(panel); 
		
		Dimension dm = Toolkit.getDefaultToolkit().getScreenSize();
		int w = dm.width;
		int h = dm.height;
		
		setUndecorated(true);
		setBounds(0, 0, w, h);
		setAlwaysOnTop(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
		setVisible(true);
	}	
}

class myPanel extends JPanel
{
	private Robot rob = null;
    private int w,h;
    private Image img;
	
	public myPanel()
	{
		Dimension dm = Toolkit.getDefaultToolkit().getScreenSize();
		w = dm.width;
		h = dm.height;

		try
		{
			rob = new Robot();
			img = (Image)rob.createScreenCapture(new Rectangle(0,0,w,h));
		}
		catch (Exception e) {}
		

	}
	
	public void paintComponent(Graphics gr) {
		super.paintComponent(gr);
		Graphics2D gr2 = (Graphics2D)gr;
		gr2.translate(w, h);
		gr2.rotate(Math.toRadians(180));
		gr2.drawImage(img, 0, 0, w,h, null);
	}
}


